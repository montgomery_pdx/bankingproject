import React from "react";

import "./TextContent1.css";

import graphlogo from "../../assets/cob-01.jpg";
import tablogo from "../../assets/cob-02.jpg";
import laptoplogo from "../../assets/cob-03.jpg";
import monitorscreenlogo from "../../assets/cob-04.jpg";
import buildinglogo from "../../assets/cob-05.jpg";

const TextContent1 = () => {
  return (
    <div className="bgImg1 p-padding-bottom-row-2">
      <div className="container-fluid">
        <div className="row">
          <div className="col-xs-12">
            <h1 className="text-align-center header-1-font-styles">
              We can help you plan and drive a digital transformation of your
              collections function and improve loan loss rates.
            </h1>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <h1 className="text-align-center header-2-font-styles">
              We leverage our deep knowledge in financial services, customer
              journey science, multi-channel customer engagement, data
              analytics, artificial intelligence, and process automation, to:
            </h1>
          </div>
        </div>
        <div className="row end-md end-lg">
          <div className="col-xs-12 col-sm-12 col-md-8 col-lg-8">
            <div className="row">
              <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div className="imge-div-1-1">
                  <img src={graphlogo} alt="" className="img-width" />
                  <p className="align-left p-padding image-text-font-styles">
                    <b>Develop a digital collections strategy</b> that addresses
                    a future-state customer experience journey and defines a set
                    of required capabilities and investments across people,
                    process, and technology to support that strategy
                  </p>
                </div>
              </div>
              <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 margin-bottom">
                <div className="imge-div-1-1">
                  <img src={tablogo} alt="" className="img-width" />
                  <p className="align-left p-padding image-text-font-styles">
                    <b>Recommend and implement technology solutions</b> that
                    integrate into your existing architecture, including
                    artificial intelligence, machine learning, messaging, chat,
                    email, and virtual agents
                  </p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div className="imge-div-1-1">
                  <img src={laptoplogo} alt="" className="img-width" />
                  <p className="align-left p-padding image-text-font-styles">
                    <b>
                      Create customer journeys, moments of truth, and contact
                      treatment strategies
                    </b>
                    that deliver the right message at the right time to increase
                    your conversion rates and deepen brand loyalty
                  </p>
                </div>
              </div>
              <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div className="imge-div-1-1">
                  <img src={monitorscreenlogo} alt="" className="img-width" />
                  <p className="align-left p-padding p-padding-bottom-row-2 image-text-font-styles">
                    <b>Design and implement solutions</b> to support data
                    aggregation and analysis, personalization, customer
                    segmentation, and propensity-to-pay models
                  </p>
                </div>
              </div>
              <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div className="imge-div-1-1">
                  <img src={buildinglogo} alt="" className="img-width" />
                  <p className="align-left p-padding p-padding-bottom-row-2 image-text-font-styles">
                    <b>
                      Define key performance metrics, processes, rewards, skill
                      competencies, and operating models
                    </b>{" "}
                    that drive the desired behavior for your collections group
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TextContent1;
