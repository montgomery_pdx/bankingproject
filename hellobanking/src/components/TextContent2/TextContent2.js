import React from "react";
import "./TextContent2.css";

import forrester from "../../assets/forrester.png";
import Shape from "../../assets/Shape.svg";

const TextContent2 = () => {
  return (
    <div className="bgImg-text-content-2">
      <div className="container-fluid container-top-bottom-padding">
        <div className="row center-xs center-sm center-md center-lg">
          <div className="col-xs-12">
            <h1 className="text-align-center">
              <img src={Shape} />
            </h1>
          </div>
        </div>
        <div className="row center-xs center-sm center-md center-lg">
          <div className="col-xs-12">
            <h1 className="text-align-center textcontent2-font-styles">
              "Banks must focus on customers' financial
              <br />
              well-being, revamp their collection
              <br />
              journeys, and advance their debt
              <br />
              management solutions."
            </h1>
          </div>
        </div>
        <div className="row center-xs center-sm center-md center-lg">
          <div className="col-12">
            <h1 className="text-align-center">
              <img className="forrester-dimension" src={forrester} alt="" />
            </h1>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TextContent2;
