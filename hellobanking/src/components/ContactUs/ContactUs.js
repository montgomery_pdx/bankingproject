import React, { Fragment, useState, useEffect } from "react";
import "./ContactUs.css";

const ContactUs = () => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [fromEmail, setFromEmail] = useState("");
  const [message, setMessage] = useState("");
  const handleFormSubmitClick = () => {
    console.log("On click triggered");
    const formData = {
      "firstName": firstName,
      "lastName": lastName,
      "companyName": companyName,
      "phoneNumber": phoneNumber,
      "fromEmail": fromEmail,
      "message": message
    }
    console.log("formData", formData);
  }
  return (
    <Fragment>
      <div className="bgImg-contact-us">
        <div className="container-fluid">
          <div className="row">
            <div className="col-xs-12">
              <h1 className="text-align-center contact-form-heading-font-style">
                Contact Us Today
              </h1>
            </div>
          </div>
          <div className="row center-xs">
            <div className="col-xs-12 col-sm-12 col-md-9 col-lg-9">
              <div className="row">
                <div className="col-xs-12">
                  <h1 className="text-align-center contact-form-text-1-font-style">
                    Financial institutions expect delinquencies and losses to
                    increase significantly once government relief programs end.
                    Now is the time to make sure you have the right people,
                    processes, and technology in place to mitigate the impact.
                  </h1>
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12">
                  <h3 className="text-align-center contact-form-text-2-font-style">
                    Please fill out this form to contact Byron Gifford,
                    Perficient’s digital debt collections expert, to discuss how
                    we can help you achieve the desired level of improvement in
                    annual loan losses. You can also email{" "}
                    <a
                      href={
                        "mailto:byron.gifford@Perficient.com?subject=Subject"
                      }
                      className="contact-form-email-link-font-style"
                    >
                      byron.gifford@Perficient.com
                    </a>
                    .
                  </h3>
                </div>
              </div>
              <div className="row center-xs">
                <div className="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                  <div className="row start-xs form-label-row-padding">
                    <div className="col-xs-6">
                      <label className="contact-form-label-font-style">
                        First Name*
                      </label>
                    </div>
                    <div className="col-xs-6">
                      <label className="contact-form-label-font-style">
                        Last Name*
                      </label>
                    </div>
                  </div>
                  <div className="row start-xs form-input-row-margin-bottom">
                    <div className="col-xs-6">
                      <input className="contact-form-input-style" 
                      value={firstName}
                      onChange={(e) =>
                        setFirstName(e.currentTarget.value)
                      }
                      ></input>
                    </div>
                    <div className="col-xs-6">
                      <input className="contact-form-input-style"
                      value={lastName}
                      onChange={(e) =>
                        setLastName(e.currentTarget.value)
                      }
                      ></input>
                    </div>
                  </div>
                  <div className="row start-xs form-label-row-padding">
                    <div className="col-xs-6">
                      <label className="contact-form-label-font-style">
                        Company*
                      </label>
                    </div>
                  </div>
                  <div className="row start-xs form-input-row-margin-bottom">
                    <div className="col-xs-6">
                      <input className="contact-form-input-style"
                      value={companyName}
                      onChange={(e) =>
                        setCompanyName(e.currentTarget.value)
                      }
                      ></input>
                    </div>
                  </div>
                  <div className="row start-xs form-label-row-padding">
                    <div className="col-xs-6">
                      <label className="contact-form-label-font-style">
                        Phone*
                      </label>
                    </div>
                    <div className="col-xs-6">
                      <label className="contact-form-label-font-style">
                        Email*
                      </label>
                    </div>
                  </div>
                  <div className="row start-xs form-input-row-margin-bottom">
                    <div className="col-xs-6">
                      <input className="contact-form-input-style"
                      value={phoneNumber}
                      onChange={(e) =>
                        setPhoneNumber(e.currentTarget.value)
                      }
                      ></input>
                    </div>
                    <div className="col-xs-6">
                      <input className="contact-form-input-style"
                      value={fromEmail}
                      onChange={(e) =>
                        setFromEmail(e.currentTarget.value)
                      }
                      ></input>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="contact-us-section-2-bg">
        <div className="container-fluid">
          <div className="row center-xs">
            <div className="col-xs-12 col-sm-12 col-md-9 col-lg-9">
              <div className="row center-xs">
                <div className="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                  <div className="contact-us-section-2">
                    <div className="row start-xs form-label-row-padding">
                      <div className="col-xs-6">
                        <label className="contact-form-label-font-style">
                          Message
                        </label>
                      </div>
                    </div>
                    <div className="row start-xs form-input-row-margin-bottom">
                      <div className="col-xs-12">
                        <textarea
                          className="contact-form-textArea-style"
                          rows="10"
                          value={message}
                          onChange={(e) =>
                            setMessage(e.currentTarget.value)
                          }
                        ></textarea>
                      </div>
                    </div>
                    <div className="row start-xs form-input-row-margin-bottom">
                      <div className="col-xs-12">
                        <button className="contact-form-submit-button" onClick={handleFormSubmitClick}>
                          Submit
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default ContactUs;
