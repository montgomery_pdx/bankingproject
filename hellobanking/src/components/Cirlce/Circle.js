import React, { useState } from "react";
import "./Circle.css";

import digital_capabilities_on from "../../assets/6-on.png";
import digital_capabilities_off from "../../assets/6-off.png";
import digital_capabilities_center from "../../assets/6-on-center.png";
import segmentation_on from "../../assets/1-on.png";
import segmentation_off from "../../assets/1-off.png";
import segmentation_center from "../../assets/1-on-center.png";
import sequencing_on from "../../assets/2-on.png";
import sequencing_off from "../../assets/2-off.png";
import sequencing_center from "../../assets/2-on-center.png";
import reporting_on from "../../assets/3-on.png";
import reporting_off from "../../assets/3-off.png";
import reporting_center from "../../assets/3-on-center.png";
import optimization_on from "../../assets/4-on.png";
import optimization_off from "../../assets/4-off.png";
import optimization_center from "../../assets/4-on-center.png";
import personalization_on from "../../assets/5-on.png";
import personalization_off from "../../assets/5-off.png";
import personalization_center from "../../assets/5-on-center.png";

const initialCenterText =
  "Communication channels, including SMS/text, email, chat, online banking/app alerts, and outbound automated interactive voice response (IVR), which can help exceed today’s customer expectations";
const Circle = () => {
  const [centerContent, setCenterContent] = useState(initialCenterText);
  const [centerImage, setCenterImage] = useState(digital_capabilities_center);
  const [digCapIcon, setDigCapIcon] = useState(digital_capabilities_on);
  const [segmentationIcon, setSegmentationIcon] = useState(segmentation_off);
  const [sequencingIcon, setSequencingIcon] = useState(sequencing_off);
  const [reportingIcon, setReportingIcon] = useState(reporting_off);
  const [optimizationIcon, setOptimizationIcon] = useState(optimization_off);
  const [personalizationIcon, setPersonalizationIcon] =
    useState(personalization_off);
  const [centerTitle, setCenterTitle] = useState("Digital Capabilities");
  const hitMe = (id) => {
    if (id === "digital_capabilities") {
      setSegmentationIcon(segmentation_off);
      setSequencingIcon(sequencing_off);
      setReportingIcon(reporting_off);
      setOptimizationIcon(optimization_off);
      setPersonalizationIcon(personalization_off);
      setCenterContent(initialCenterText);
      setCenterImage(digital_capabilities_center);
      setDigCapIcon(digital_capabilities_on);
      setCenterTitle("Digital Capabilities");
    }
    if (id === "segmentation") {
      setDigCapIcon(digital_capabilities_off);
      setSequencingIcon(sequencing_off);
      setReportingIcon(reporting_off);
      setOptimizationIcon(optimization_off);
      setPersonalizationIcon(personalization_off);
      setSegmentationIcon(segmentation_on);
      setCenterImage(segmentation_center);
      setCenterTitle("Segmentation");
      setCenterContent(
        "Design modeling that leverages multi-dimensional risk factors, customer personas, and activity insights"
      );
    }
    if (id === "sequencing") {
      setDigCapIcon(digital_capabilities_off);
      setSegmentationIcon(segmentation_off);
      setReportingIcon(reporting_off);
      setOptimizationIcon(optimization_off);
      setPersonalizationIcon(personalization_off);
      setSequencingIcon(sequencing_on);
      setCenterTitle("Sequencing ");
      setCenterContent(
        "Contact treatment strategies based on customer intelligence data that predict and use targeted behavior risk to deploy activity"
      );
      setCenterImage(sequencing_center);
    }
    if (id === "reporting") {
      setDigCapIcon(digital_capabilities_off);
      setSegmentationIcon(segmentation_off);
      setSequencingIcon(sequencing_off);
      setOptimizationIcon(optimization_off);
      setPersonalizationIcon(personalization_off);
      setReportingIcon(reporting_on);
      setCenterTitle("Reporting and Analytics");
      setCenterContent(
        "Performance dashboards, predictive analytics, and classification modeling that supports testing, decisions, and competency improvements for holistic transformation"
      );
      setCenterImage(reporting_center);
    }
    if (id === "processOptimization") {
      setDigCapIcon(digital_capabilities_off);
      setSegmentationIcon(segmentation_off);
      setSequencingIcon(sequencing_off);
      setReportingIcon(reporting_off);
      setPersonalizationIcon(personalization_off);
      setOptimizationIcon(optimization_on);
      setCenterTitle("Process Optimization");
      setCenterContent(
        "Automate processes that accelerate performance and long-term customer loyalty and retention"
      );
      setCenterImage(optimization_center);
    }
    if (id === "personalization") {
      setDigCapIcon(digital_capabilities_off);
      setSegmentationIcon(segmentation_off);
      setSequencingIcon(sequencing_off);
      setReportingIcon(reporting_off);
      setOptimizationIcon(optimization_off);
      setPersonalizationIcon(personalization_on);
      setCenterTitle("Personalization");
      setCenterContent(
        "Rules-based prescriptive and personalized messaging with call-to-actions and proactive marketing of payment relief offerings"
      );
      setCenterImage(personalization_center);
    }
  };
  return (
    <div className="bgImgCircle text-align-center">
      <div className="container-fluid container-top-bottom-padding">
        <div className="row center-xs center-sm center-md center-lg">
          <div className="col-12">
            <h1 className="text-align-center heading-style">
              Elements of a Digital Debt Collection Transformation
            </h1>
          </div>
        </div>
        <div className="row center-xs center-sm center-md center-lg">
          <div className="col-xs-6">
            <svg viewBox="0 0 520 520" className="padding-bottom">
              <circle
                cx="260"
                cy="260"
                r="230"
                className="text-align-center"
                fillOpacity=".1"
                strokeDasharray="10.71, 7"
                stroke="#b69967"
                strokeWidth="3"
              />
              <foreignObject
                x="53"
                y="80"
                height="15%"
                width="15%"
                onClick={() => {
                  hitMe("digital_capabilities");
                }}
              >
                <img className="myWidget" src={digCapIcon} width="50" alt="" />
              </foreignObject>
              <foreignObject
                x="390"
                y="75"
                height="15%"
                width="15%"
                onClick={() => {
                  hitMe("segmentation");
                }}
              >
                <img
                  className="myWidget"
                  src={segmentationIcon}
                  width="50"
                  alt=""
                />
              </foreignObject>
              <foreignObject
                x="450"
                y="245"
                height="15%"
                width="15%"
                onClick={() => {
                  hitMe("sequencing");
                }}
              >
                <img
                  className="myWidget"
                  src={sequencingIcon}
                  width="50"
                  alt=""
                />
              </foreignObject>
              <foreignObject
                x="0"
                y="260"
                height="15%"
                width="15%"
                onClick={() => {
                  hitMe("personalization");
                }}
              >
                <img
                  className="myWidget"
                  src={personalizationIcon}
                  width="50"
                  alt=""
                />
              </foreignObject>
              {/* Modify from here */}
              <foreignObject
                x="53"
                y="400"
                height="15%"
                width="15%"
                onClick={() => {
                  hitMe("processOptimization");
                }}
              >
                <img
                  className="myWidget"
                  src={optimizationIcon}
                  width="50"
                  alt=""
                />
              </foreignObject>
              <foreignObject
                x="390"
                y="400"
                height="15%"
                width="15%"
                onClick={() => {
                  hitMe("reporting");
                }}
              >
                <img
                  className="myWidget"
                  src={reportingIcon}
                  width="50"
                  alt=""
                />
              </foreignObject>
              <foreignObject
                x="140"
                y="130"
                height="300px"
                width="250px"
                //   className="text-align-center"
              >
                <img src={centerImage} alt="" />
                <div className="row center-xs center-sm center-md center-lg">
                  <div className="col-xs-12">
                    <p className="center-title-style">{centerTitle}</p>
                    <p className="content-padding font-size-12">
                      {centerContent}
                    </p>
                  </div>
                </div>
              </foreignObject>
            </svg>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Circle;
