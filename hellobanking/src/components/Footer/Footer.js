import React, { Fragment } from "react";
import "./Footer.css";
import PRFTLOGO from "../../assets/prft-logo-white.svg";
import { AiOutlineLinkedin, AiOutlineYoutube } from 'react-icons/ai';
import { FiTwitter } from "react-icons/fi";
import { RiFacebookCircleLine, } from "react-icons/ri";
import { FaBlog } from "react-icons/fa";
import { IconContext } from "react-icons";

const Footer = () => {
  return (
    <Fragment>
      <div className="footer-background-color text-color-white">
        <div className="container-fluid footer-content-padding">
          <div className="row middle-xs">
            <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
              <div className="row">
                <div className="col-xs-12"><img src={PRFTLOGO}  alt="" className="footer-prft-logo"/></div>
              </div>
              <div className="row">
                <div className="col-xs-2">
                  <IconContext.Provider value={{ className: "external-footer-icons" }}>
                    <div>
                      <AiOutlineLinkedin />
                    </div>
                  </IconContext.Provider>
                </div>
                <div className="col-xs-2">Youtube</div>
                <div className="col-xs-2">Twitter</div>
                <div className="col-xs-2">Facebook</div>
                <div className="col-xs-3">PRFTBlogs</div>
              </div>
            </div>
            <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
              <div className="row">
                <div className="col-xs-12">
                  <p className="gold-text">GET TO KNOW US</p>
                </div>
              </div>
              <div className="row">
                <div className="col-xs-6">
                  <a
                    href="https://www.perficient.com/who-we-are"
                    className="text-color-white"
                    style={{ textDecoration: "none" }}
                  >
                    Who we are
                  </a>
                </div>
                <div className="col-xs-6">
                  <a
                    href="https://www.perficient.com/careers"
                    className="text-color-white"
                    style={{ textDecoration: "none" }}
                  >
                    Careers
                  </a>
                </div>
              </div>
              <div className="row footer-list-padding">
                <div className="col-xs-6">
                  <a
                    href="https://www.perficient.com/what-we-do"
                    className="text-color-white"
                    style={{ textDecoration: "none" }}
                  >
                    What we do
                  </a>
                </div>
                <div className="col-xs-6">
                  <a
                    href="https://perficient.gcs-web.com/"
                    className="text-color-white"
                    style={{ textDecoration: "none" }}
                  >
                    Investors
                  </a>
                </div>
              </div>
              <div className="row footer-list-padding">
                <div className="col-xs-6">
                  <a
                    href="https://www.perficient.com/success-stories"
                    className="text-color-white"
                    style={{ textDecoration: "none" }}
                  >
                    Success Stories
                  </a>
                </div>
                <div className="col-xs-6">
                  <a
                    href="https://www.perficient.com/news-room"
                    className="text-color-white"
                    style={{ textDecoration: "none" }}
                  >
                    News Room
                  </a>
                </div>
              </div>
              <div className="row footer-list-padding">
                <div className="col-xs-6">
                  <a
                    href="https://www.perficient.com/insights"
                    className="text-color-white"
                    style={{ textDecoration: "none" }}
                  >
                    Insights
                  </a>
                </div>
                <div className="col-xs-6">
                  <a
                    href="https://www.perficient.com/contact"
                    className="text-color-white"
                    style={{ textDecoration: "none" }}
                  >
                    Contact
                  </a>
                </div>
              </div>
            </div>
            <div className="col-xs-12 col-sm-12 col-md-5 col-lg-5">
              <div className="footer-right-border">
                <div className="row">
                  <div className="col-xs-12">
                    <p className="gold-text">EXPLORE INDUSTRIES</p>
                  </div>
                </div>
                <div className="row">
                  <div className="col-xs-6">
                    <a
                      href="https://www.perficient.com/industries/automotive"
                      className="text-color-white"
                      style={{ textDecoration: "none" }}
                    >
                      Automotive
                    </a>
                  </div>
                  <div className="col-xs-6">
                    <a
                      href="https://www.perficient.com/industries/healthcare"
                      className="text-color-white"
                      style={{ textDecoration: "none" }}
                    >
                      Healthcare
                    </a>
                  </div>
                </div>
                <div className="row footer-list-padding">
                  <div className="col-xs-6">
                    <a
                      href="https://www.perficient.com/industries/consumer-markets"
                      className="text-color-white"
                      style={{ textDecoration: "none" }}
                    >
                      Consumer Markets
                    </a>
                  </div>
                  <div className="col-xs-6">
                    <a
                      href="https://www.perficient.com/industries/life-sciences"
                      className="text-color-white"
                      style={{ textDecoration: "none" }}
                    >
                      Life Sciences
                    </a>
                  </div>
                </div>
                <div className="row footer-list-padding">
                  <div className="col-xs-6">
                    <a
                      href="https://www.perficient.com/industries/energy-and-utilities"
                      className="text-color-white"
                      style={{ textDecoration: "none" }}
                    >
                      Energy + Utilities
                    </a>
                  </div>
                  <div className="col-xs-6">
                    <a
                      href="https://www.perficient.com/industries/manufacturing"
                      className="text-color-white"
                      style={{ textDecoration: "none" }}
                    >
                      Manufacturing
                    </a>
                  </div>
                </div>
                <div className="row footer-list-padding">
                  <div className="col-xs-6">
                    <a
                      href="https://www.perficient.com/industries/financial-services"
                      className="text-color-white"
                      style={{ textDecoration: "none" }}
                    >
                      Financial Services
                    </a>
                  </div>
                  <div className="col-xs-6">
                    <a
                      href="https://www.perficient.com/industries/telecommunications"
                      className="text-color-white"
                      style={{ textDecoration: "none" }}
                    >
                      Telecommunications
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid terms-and-condition-background text-color-white terms-and-condition-padding">
        <div className="row start-xs">
          <div className="col-xs-12 col-sm-12 col-md-7 col-lg-7">
            <div className="row">
              <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <p className="right-solid-border">
                  © 2021 Perficient Inc, All Rights Reserved
                </p>
              </div>
              <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                <div className="a-tag-styles right-solid-border">
                  <a
                    href="https://www.perficient.com/privacy-policy"
                    className="text-color-white text-align-center"
                    style={{ textDecoration: "none" }}
                  >
                    Privacy Policy
                  </a>
                </div>
              </div>
              <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div className="a-tag-styles">
                  <a
                    href="https://www.perficient.com/customer-security-statement"
                    className="text-color-white text-align-center"
                    style={{ textDecoration: "none" }}
                  >
                    Customer Security Statement
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Footer;
