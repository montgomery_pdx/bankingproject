import React, { Fragment } from "react";
import "./LandingPage.css";
import heroOverlay from "../../assets/hero-overlay.png";
import more from "../../assets/MORE.svg";
import PRFTLOGO from "../../assets/prft-logo-white.svg";

const LandingPage = ({ reference, click }) => {
  return (
    <Fragment>
      <div className="landingPageImg text-align-center text-color-white">
        <div className="container-fluid container-top-bottom-padding">
          <div className="row between-xs">
            <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
              <h1 className="">
                <img src={PRFTLOGO} />
              </h1>
            </div>
            {/* <div className="col-xs-8"></div> */}
            <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
              <h1 className="landing-page-header-top-right-fonts">
                PERFICIENT.COM
              </h1>
            </div>
          </div>
          <div className="row start-xs row-bottom-padding">
            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <img className="hero-overlay-padding" src={heroOverlay} alt="" />
            </div>
            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div className="row">
                <h1 className="component-header-font-white">
                  A Pathway to <br></br>
                  Improved losses and <br></br>Happier Customers
                </h1>
              </div>
              <div className="row" ref={reference}>
                <button className="landing-page-roi-button" onClick={click}>
                  Calculate Your ROI
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid container-top-bottom-padding">
        <div className="row center-xs center-sm center-md center-lg">
          <div className="col-xs-12">
            <h1 className="text-align-center landing-page-header-2-fonts">
              Financial services companies are rushing to implement strategies
              and technologies to streamline debt collection while also trying
              to make it more human and conversational.
            </h1>
          </div>
        </div>
        <div className="row center-xs center-sm center-md center-lg">
          <div className="col-xs-12">
            <h2 className="text-align-center landing-page-header-3-fonts">
              Communicating with customers over their preferred digital channels
              is highly effective in debt repayment. In fact, up to 92% of
              customers make partial or full payments when firms leverage online
              and mobile personalization strategies.
            </h2>
          </div>
        </div>
        <div className="row center-xs center-sm center-md center-lg">
          <div className="col-xs-12">
            <h1 className="text-align-center">
              <img src={more} />
            </h1>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default LandingPage;
