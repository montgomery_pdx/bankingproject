const database = {
  "ally-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "65347000000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.8004",
    "name": "ally-bank"
  },

  "american-express-national-bank": {
    "loans_credit_card_loans": "59365087000",
    "loans_loans_to_individuals": "60258719000",
    "chargeoff_credit_card_loans": "1.2972",
    "chargeoff_loans_to_individuals": "1.3616", "name": "American Express Nation Bank"

  },
  "ameris-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "739729000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.5449", "name": "Ameris Bank"
  },
  "arvest-bank": {
    "loans_credit_card_loans": "145149000",
    "loans_loans_to_individuals": "1683912000",
    "chargeoff_credit_card_loans": "2.5781",
    "chargeoff_loans_to_individuals": "1.6226", "name": "Arvest Bank"
  },
  "atlantic-union-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "622060000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.5828", "name": "Atlantic Union Bank"
  },
  "banco-popular-de-puerto-rico": {
    "loans_credit_card_loans": "878000000",
    "loans_loans_to_individuals": "5347000000",
    "chargeoff_credit_card_loans": "1.3348",
    "chargeoff_loans_to_individuals": "0.5993", "name": "Banco Popular de Puerto Rico"
  },
  "bank-of-america": {
    "loans_credit_card_loans": "72786000000",
    "loans_loans_to_individuals": "144973000000",
    "chargeoff_credit_card_loans": "3.3480",
    "chargeoff_loans_to_individuals": "1.7997", "name": "Bank of America"
  },
  "bank-of-hawaii": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "1004675000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "1.6928", "name": "Bank of Hawaii"
  },
  "bank-of-the-west-3514": {
    "loans_credit_card_loans": "243080000",
    "loans_loans_to_individuals": "14016428000",
    "chargeoff_credit_card_loans": "5.7713",
    "chargeoff_loans_to_individuals": "0.5019", "name": "Bank of the West"
  },
  "bank-ozk": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "2275007000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.3824", "name": "Bank OZK"
  },
  "bbva-usa": {
    "loans_credit_card_loans": "812242000",
    "loans_loans_to_individuals": "6864206000",
    "chargeoff_credit_card_loans": "8.4688",
    "chargeoff_loans_to_individuals": "3.2028", "name": "BBVA USA"
  },
  "bmo-harris-bank": {
    "loans_credit_card_loans": "356492000",
    "loans_loans_to_individuals": "7501337000",
    "chargeoff_credit_card_loans": "3.9332",
    "chargeoff_loans_to_individuals": "0.3951", "name": "BMO Harris Bank"
  },
  "bny-mellon": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "2815000000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.0000", "name": "BNY Mellon"
  },
  "capital-one-bank-usa": {
    "loans_credit_card_loans": "77200610000",
    "loans_loans_to_individuals": "77200610000",
    "chargeoff_credit_card_loans": "2.7561",
    "chargeoff_loans_to_individuals": "2.7561", "name": "Capital One Bank USA"
  },
  "citibank": {
    "loans_credit_card_loans": "131884000000",
    "loans_loans_to_individuals": "153487000000",
    "chargeoff_credit_card_loans": "3.1377",
    "chargeoff_loans_to_individuals": "2.9162", "name": "Citibank"
  },
  "citizens-business-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "15687000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.0000", "name": "Citizens Business Bank"
  },
  "citizens-community-federal": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "35167000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.1076", "name": "Citizens Community Federal"
  },
  "city-national-bank-of-florida": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "45321000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "2.3859", "name": "City National Bank of Florida"
  },
  "comerica-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "526000000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.0000", "name": "Comerica Bank"
  },
  "community-trust-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "1034000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.0000", "name": "Community Trust Bank"
  },
  "crescent-bank-trust": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "687017000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "4.2581", "name": "Crescent Bank Trust"
  },
  "customers-bank": {
    "loans_credit_card_loans": "468000",
    "loans_loans_to_individuals": "1440176000",
    "chargeoff_credit_card_loans": "3.3708",
    "chargeoff_loans_to_individuals": "3.2410", "name": "Customers Bank"
  },
  "discover-bank": {
    "loans_credit_card_loans": "67144718000",
    "loans_loans_to_individuals": "84259194000",
    "chargeoff_credit_card_loans": "2.7333",
    "chargeoff_loans_to_individuals": "2.4794", "name": "Discover Bank"
  },
  "fifth-third-bank": {
    "loans_credit_card_loans": "1810459000",
    "loans_loans_to_individuals": "19181730000",
    "chargeoff_credit_card_loans": "5.3373",
    "chargeoff_loans_to_individuals": "0.8931", "name": "Fifth Third Bank"
  },
  "first-citizens-bank-4433": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "15693000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.0000", "name": "First Citizens Bank"
  },
  "first-commonwealth-bank": {
    "loans_credit_card_loans": "10901000",
    "loans_loans_to_individuals": "852511000",
    "chargeoff_credit_card_loans": "5.1932",
    "chargeoff_loans_to_individuals": "0.5812", "name": "First Commonwealth Bank"
  },
  "first-hawaiian-bank": {
    "loans_credit_card_loans": "193140000",
    "loans_loans_to_individuals": "1192394000",
    "chargeoff_credit_card_loans": "2.4308",
    "chargeoff_loans_to_individuals": "1.1152", "name": "First Hawaiin Bank"
  },
  "first-interstate-bank": {
    "loans_credit_card_loans": "64604000",
    "loans_loans_to_individuals": "985248000",
    "chargeoff_credit_card_loans": "2.0767",
    "chargeoff_loans_to_individuals": "0.5467", "name": "First Interstate Bank"
  },
  "first-national-bank-of-omaha": {
    "loans_credit_card_loans": "5470116000",
    "loans_loans_to_individuals": "6141540000",
    "chargeoff_credit_card_loans": "3.4917",
    "chargeoff_loans_to_individuals": "3.3581", "name": "First National Bank of Omaha"
  },
  "first-national-bank-of-pennsylvania": {
    "loans_credit_card_loans": "6687000",
    "loans_loans_to_individuals": "1331379000",
    "chargeoff_credit_card_loans": "4.1995",
    "chargeoff_loans_to_individuals": "0.1864", "name": "First National Bank of Pennsylvania"
  },
  "first-premier-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "933838000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.1884", "name": "First Premier Bank"
  },
  "first-republic-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "5328706000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.0189", "name": "First Republic Bank"
  },
  "flagstar-bank-fsb": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "1049187000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.2595", "name": "Flagstar Bank FSB"
  },
  "gate-city-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "731601000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.4245", "name": "Gate City Bank"
  },
  "goldman-sachs-bank-usa": {
    "loans_credit_card_loans": "4376000000",
    "loans_loans_to_individuals": "13235000000",
    "chargeoff_credit_card_loans": "2.4057",
    "chargeoff_loans_to_individuals": "1.8336", "name": "Goldman Sachs Bank USA"
  },
  "hancock-whitney-bank": {
    "loans_credit_card_loans": "78836000",
    "loans_loans_to_individuals": "754593000",
    "chargeoff_credit_card_loans": "2.5839",
    "chargeoff_loans_to_individuals": "0.9962", "name": "Hancock Whitney Bank"
  },
  "hsbc-bank-usa": {
    "loans_credit_card_loans": "937396000",
    "loans_loans_to_individuals": "1238627000",
    "chargeoff_credit_card_loans": "6.6116",
    "chargeoff_loans_to_individuals": "6.1646", "name": "HSBC Bank USA"
  },
  "jpmorgan-chase-bank-locations": {
    "loans_credit_card_loans": "NILL",
    "loans_loans_to_individuals": "NILL",
    "chargeoff_credit_card_loans": "NILL",
    "chargeoff_loans_to_individuals": "NILL", "name": "JPMorgan Case Bank Locations"
  },
  "keybank": {
    "loans_credit_card_loans": "909302000",
    "loans_loans_to_individuals": "10778079000",
    "chargeoff_credit_card_loans": "1.7755",
    "chargeoff_loans_to_individuals": "0.4696", "name": "Keybank"
  },
  "landmark-community-bank-34982": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "5782000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.6765", "name": "Landmark Community Bank"
  },
  "manufacturers-and-traders-trust-company": {
    "loans_credit_card_loans": "509870000",
    "loans_loans_to_individuals": "12894263000",
    "chargeoff_credit_card_loans": "2.6213",
    "chargeoff_loans_to_individuals": "0.4789", "name": "Manufacturers and Traders Trust Company"
  },
  "meadows-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "39000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.0000", "name": "Meadows Bank"
  },
  "merrick-bank": {
    "loans_credit_card_loans": "2722654000",
    "loans_loans_to_individuals": "3499261000",
    "chargeoff_credit_card_loans": "7.3285",
    "chargeoff_loans_to_individuals": "6.2931", "name": "Merrick Bank"
  },
  "midland-states-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "813194000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "-0.0034", "name": "Midland States Bank"
  },
  "morgan-stanley-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "8590000000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.0000", "name": "Morgan Stanley Bank"
  },
  "nbt-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "1512608000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.5957", "name": "NBT Bank"
  },
  "new-york-community-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "1828000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "2.2426", "name": "New York Community Bank"
  },
  "northwest-bank-28178": {
    "loans_credit_card_loans": "49815000",
    "loans_loans_to_individuals": "1554925000",
    "chargeoff_credit_card_loans": "2.6736",
    "chargeoff_loans_to_individuals": "0.3578", "name": "Northwest Bank"
  },
  "oriental-bank": {
    "loans_credit_card_loans": "52067000",
    "loans_loans_to_individuals": "1912115000",
    "chargeoff_credit_card_loans": "9.3040",
    "chargeoff_loans_to_individuals": "1.5271", "name": "Oriental Bank"
  },
  "pacific-western-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "338307000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.4098", "name": "Pacific Western Bank"
  },
  "peoples-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "1476000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.0000", "name": "Peoples Bank"
  },
  "regions-bank": {
    "loans_credit_card_loans": "1111000000",
    "loans_loans_to_individuals": "5285000000",
    "chargeoff_credit_card_loans": "3.0981",
    "chargeoff_loans_to_individuals": "2.3922", "name": "Regions Bank"
  },
  "sallie-mae-bank": {
    "loans_credit_card_loans": "11404000",
    "loans_loans_to_individuals": "21547330000",
    "chargeoff_credit_card_loans": "2.9142",
    "chargeoff_loans_to_individuals": "0.8469", "name": "Sallie Mae Bank"
  },
  "south-state-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "880136000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.7385", "name": "South State Bank"
  },
  "southeast-bank": {
    "loans_credit_card_loans": "312000",
    "loans_loans_to_individuals": "1184097000",
    "chargeoff_credit_card_loans": "12.5535",
    "chargeoff_loans_to_individuals": "0.0583", "name": "Southeast Bank"
  },
  "synchrony-bank": {
    "loans_credit_card_loans": "65904000000",
    "loans_loans_to_individuals": "68221000000",
    "chargeoff_credit_card_loans": "3.6706",
    "chargeoff_loans_to_individuals": "3.6173", "name": "Synchrony Bank"
  },
  "synovus-bank": {
    "loans_credit_card_loans": "181554000",
    "loans_loans_to_individuals": "2460829000",
    "chargeoff_credit_card_loans": "3.0173",
    "chargeoff_loans_to_individuals": "0.7282", "name": "Synovus Bank"
  },
  "tcf-national-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "1145370000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "-0.0569", "name": "TCF National Bank"
  },
  "td-bank": {
    "loans_credit_card_loans": "4198464000",
    "loans_loans_to_individuals": "29572752000",
    "chargeoff_credit_card_loans": "2.9542",
    "chargeoff_loans_to_individuals": "1.2580", "name": "TD Bank"
  },
  "the-bancorp-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "1536065000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.0043", "name": "The Bancorp Bank"
  },
  "the-canandaigua-national-bank-and-trust-company": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "772231000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.2469", "name": "The Candaigua National Bank and Trust Company"
  },
  "the-citizens-bank-of-edmond": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "1286000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "-1.2121", "name": "The Citizens Bank of Edmond"
  },
  "the-park-national-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "1660115000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.0914", "name": "The Park National Bank"
  },
  "towne-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "313154000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.1566", "name": "Towne Bank"
  },
  "truist-bank": {
    "loans_credit_card_loans": "3245000000",
    "loans_loans_to_individuals": "53760000000",
    "chargeoff_credit_card_loans": "3.2893",
    "chargeoff_loans_to_individuals": "0.7038", "name": "Truist Bank"
  },
  "u-s-bank": {
    "loans_credit_card_loans": "20872672000",
    "loans_loans_to_individuals": "58639276000",
    "chargeoff_credit_card_loans": "2.6560",
    "chargeoff_loans_to_individuals": "1.2268", "name": "USBank"
  },
  "ubs-bank-usa": {
    "loans_credit_card_loans": "199321000",
    "loans_loans_to_individuals": "32920254000",
    "chargeoff_credit_card_loans": "0.6645",
    "chargeoff_loans_to_individuals": "0.0045", "name": "UBS Bank USA"
  },
  "unified-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "7480000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "-0.0514", "name": "Unified Bank"
  },
  "union-bank-trust-company-1753": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "11881000",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "0.2408", "name": "Union Bank Trust Company"
  },
  "united-bank-22858": {
    "loans_credit_card_loans": "7999000",
    "loans_loans_to_individuals": "1177428000",
    "chargeoff_credit_card_loans": "1.3698",
    "chargeoff_loans_to_individuals": "0.2119", "name": "United Bank"
  },
  "usaa-savings-bank": {
    "loans_credit_card_loans": "0",
    "loans_loans_to_individuals": "0",
    "chargeoff_credit_card_loans": "NA",
    "chargeoff_loans_to_individuals": "NA", "name": "USAA Savings Bank"
  },
  "zions-bancorporation-n-a-": {
    "loans_credit_card_loans": "NILL",
    "loans_loans_to_individuals": "NILL",
    "chargeoff_credit_card_loans": "NILL",
    "chargeoff_loans_to_individuals": "NILL", "name": "Zions Bancorporation NA"
  }
}
module.exports = { database };
