import React, { useState, useEffect } from "react";

import "./calculatorApp.css";
import { bankNamesArray } from "./BankData.js";
import { calculator } from "./logic.js";

const CalculatorApp = ({ reference }) => {
  const [improvementValues, setImprovementValues] = useState(0.01);
  const [amountToInvest, setAmountToInvest] = useState(500000);
  const [improvementResults, setImprovementResults] = useState();
  const [paybackPeriod, setPaybackPeriod] = useState();
  const [bankNameStateVariable, setBankNameStateVariable] = useState("");
  const { database } = require("./loanAndChargeOffData");

  useEffect(() => {
    let bankName = getBankNameFromUrl();
    if (bankName !== null && bankName !== undefined) {
      let calulatedValues = calculator(
        bankName,
        amountToInvest,
        improvementValues
      );
      setImprovementResults(calulatedValues.improvement);
      setPaybackPeriod(calulatedValues.months);
    }
  }, [amountToInvest, improvementValues]);
  const getBankNameFromUrl = () => {
    let digcolUrl = window.location.search;
    const urlParams = new URLSearchParams(digcolUrl);
    const bankNameFromURL = urlParams.get("bankname");
    if (bankNameFromURL !== undefined || bankNameFromURL !== null) {
      if (database.hasOwnProperty(bankNameFromURL)) {
        const obj = database[bankNameFromURL].name;
        setBankNameStateVariable(obj);
        return bankNamesArray[bankNameFromURL];
      } else {
        alert(`The bankName of '${bankNameFromURL}' is not in our datastore.`);
      }
    }
    return undefined;
  };

  const dollarize = (amount) => {
    if (amount) {
      amount = amount.toFixed(0);
      const formatter = new Intl.NumberFormat("en-US", {
        style: "currency",
        currency: "USD",
      });

      let money = formatter.format(amount); /* $2,500.00 */
      return money;
    }
  };

  return (
    <div className="bgImgCalculator text-align-center" ref={reference}>
      <div className="container-fluid container-top-bottom-padding">
        <div className="row">
          <div className="col-xs-12">
            <h1 className="text-align-center component-header-font-white">
              Loan Loss Improvement ROI Calculator
            </h1>
          </div>
        </div>
        <div className="row center-xs">
          <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div className="row start-xs">
              <div className="col-xs-12">
                <div className="row start-xs">
                  <h2 className="text-align-center Your-Potential-ROI-fonts">
                    Your Potential ROI:
                  </h2>
                </div>
                <div className="row start-xs bottom-xs">
                  <p className="text-align-center calculator-label-font-style financial-institution-margin">
                    Financial Institution
                  </p>
                </div>
                <div className="row start-xs top-xs">
                  <h3 className="text-color-white text-align-left bank-name-margin bank-name-font">
                    {bankNameStateVariable}
                  </h3>
                </div>
                <div className="row start-xs form-label-row-padding">
                  <div className="col-xs-6">
                    <label className="calculator-label-font-style">
                      Desired improvement in annual Loan losses
                    </label>
                  </div>
                  <div className="col-xs-6">
                    <label className="calculator-label-font-style">
                      Improvement in annual Loan losses
                    </label>
                  </div>
                </div>
                <div className="row start-xs improvement-row-margin-bottom">
                  <div className="col-xs-6">
                    <select
                      className="select-label-baseStyle"
                      value={improvementValues}
                      onChange={(e) =>
                        setImprovementValues(e.currentTarget.value)
                      }
                    >
                      <option value="0.01">1%</option>
                      <option value="0.02">2%</option>
                      <option value="0.03">3%</option>
                      <option value="0.04">4%</option>
                      <option value="0.05">5%</option>
                      <option value="0.06">6%</option>
                      <option value="0.07">7%</option>
                      <option value="0.08">8%</option>
                      <option value="0.09">9%</option>
                      <option value="0.1">10%</option>
                    </select>
                  </div>
                  <div className="col-xs-6">
                    <div className="row improvement-value-row-height">
                      <input
                        className="select-label-baseStyle"
                        value={dollarize(improvementResults)}
                      ></input>
                    </div>
                    <div className="row form-label-row-padding">
                      <label className="calculator-label-disclaimer-font-style">
                        *based on public data (12/31/2020)
                      </label>
                    </div>
                  </div>
                </div>
                <div className="row start-xs middle-xs form-label-row-padding">
                  <div className="col-xs-6">
                    <label className="calculator-label-font-style">
                      Level of One-Time Investment to Achieve <br></br>{" "}
                      improvement in Annual losses
                    </label>
                  </div>
                  <div className="col-xs-6">
                    <label className="calculator-label-font-style">
                      Investment Payback Period (months)
                    </label>
                  </div>
                </div>
                <div className="row start-xs improvement-row-margin-bottom payback-row-height">
                  <div className="col-xs-6">
                    <select
                      className="select-label-baseStyle"
                      value={amountToInvest}
                      onChange={(e) => setAmountToInvest(e.target.value)}
                    >
                      <option value="500000">$500,000</option>
                      <option value="1000000">$1,000,000</option>
                      <option value="1500000">$1,500,000</option>
                      <option value="2000000">$2,000,000</option>
                      <option value="2500000">$2,500,000</option>
                      <option value="3000000">$3,000,000</option>
                      <option value="3500000">$3,500,000</option>
                      <option value="4000000">$4,000,000</option>
                      <option value="4500000">$4,500,000</option>
                      <option value="5000000">$5,000,000</option>
                    </select>
                  </div>
                  <div className="col-xs-6">
                    <div className="row improvement-value-row-height">
                      <input
                        className="select-label-baseStyle"
                        value={paybackPeriod}
                      ></input>
                    </div>
                  </div>
                </div>
                <div className="row start-xs">
                  <h4 className="text-align-center calculator-footer-bottom-text-font-style">
                    **Your personalized estimate is based on publicly available
                    FDIC data and industry benchmark data. Calculations and
                    success rate will vary for each company.
                  </h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CalculatorApp;
