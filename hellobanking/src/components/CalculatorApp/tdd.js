const { calculator } = require("./logic.js");

const { bankNamesArray } = require("./BankData");

function rate_1_percent_american_express(bankName, amountToInvest, rate) {
  const actual = calculator(bankName, amountToInvest, rate);

  const expected = {
    loans: 63143190000,
    charges: 2.614,
    estimated_net_charge_off: 165056298660,
    rate: 0.01,
    improvement: 1650562986.6000001,
    amountToInvest: 500000,
    months: 0.0003,
  };

  let isOk = true;
  for (let key in expected) {
    const v1 = expected[key];
    const v2 = actual[key];
    if (v1 != v2) {
      isOk = false;
      console.log("BOO on key " + key + " expected " + v1 + " got " + v2);
    } else {
      // console.log("YAY on key " + key + " expected " + v1 + " got " + v2)
    }
  }

  if (isOk === true) {
    console.log("PASS rate_1_percent_american_express");
  } else {
    console.log("FAIL rate_1_percent_american_express");
  }
}

function rate_2_percent_american_express(bankName, amountToInvest, rate) {
  const actual = calculator(bankName, amountToInvest, rate);

  const expected = {
    loans: 63143190000,
    charges: 2.614,
    estimated_net_charge_off: 165056298660,
    rate: 0.02,
    improvement: 3301125973.2000003,
    amountToInvest: 500000,
    months: 0.0002,
  };

  let isOk = true;
  for (let key in expected) {
    const v1 = expected[key];
    const v2 = actual[key];
    if (v1 != v2) {
      isOk = false;
      console.log("BOO on key " + key + " expected " + v1 + " got " + v2);
    } else {
      //console.log("YAY on key " + key + " expected " + v1 + " got " + v2)
    }
  }

  if (isOk === true) {
    console.log("PASS rate_1_percent_american_express");
  } else {
    console.log("FAIL rate_1_percent_american_express");
  }
}
const getBankNameFromUrl = () => {
  //   let digcolUrl = window.location.search;
  //   const urlParams = new URLSearchParams(digcolUrl);
  // const bankNameFromURL = urlParams.get("bankname");
  const bankNameFromURL = "bny-mellon";
  console.log("bankName in function", bankNameFromURL);
  if (bankNameFromURL !== null) {
    try {
      let bankNameValue = bankNamesArray[bankNameFromURL];
      console.log("bankNameValue", bankNameValue);
      if (bankNameValue === undefined) {
        //   alert("Something went wrong");
      }
      // setBankName(bankNameValue);
    } catch (e) {
      // alert("No bank found");
      console.log("Error", e);
    }
  }
};

if (require.main === module) {
  const bankName = "american-express-national-bank";
  const amountToInvest = 500000;
  let rate = 0.01;
  rate_1_percent_american_express(bankName, amountToInvest, rate);

  rate = 0.02;
  rate_2_percent_american_express(bankName, amountToInvest, rate);

  getBankNameFromUrl();
}
