const { database } = require("./loanAndChargeOffData");

function getScrapedInformation(bankName) {
  // console.log(JSON.stringify(database, null, 2))
  const loanAndChargeOffValues = database[bankName];
  return loanAndChargeOffValues;

}

function calculator(bankName, amountToInvest, rate) {
  const obj = getScrapedInformation(bankName);
  // const charges = obj["chargeoff_credit_card_loans"];
  // const loans = obj["loans_credit_card_loans"];
  const charges = obj["chargeoff_loans_to_individuals"];
  const loans = obj["loans_loans_to_individuals"];
  // loans_loans_to_individuals
  // chargeoff_loans_to_individuals

  const estimated_net_charge_off = loans * charges;
  const improvement = estimated_net_charge_off * rate;
  let months = amountToInvest / improvement;
  months = months.toFixed(4);
  const result = {
    loans: loans,
    charges: charges,
    estimated_net_charge_off: estimated_net_charge_off,
    rate: rate,
    improvement: improvement,
    amountToInvest: amountToInvest,
    months: months,
  };

  return result;
}
module.exports = {
  calculator,
};
