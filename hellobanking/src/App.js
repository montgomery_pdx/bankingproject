import "./App.css";
import React, { useRef } from "react";
import TextContent1 from "./components/TextContent1/TextContent1";
import TextContent2 from "./components/TextContent2/TextContent2";
import Circle from "./components/Cirlce/Circle";
import ContactUs from "./components/ContactUs/ContactUs";
import CalculatorApp from "./components/CalculatorApp/CalculatorApp";
import Footer from "./components/Footer/Footer";
import LandingPage from "./components/LandingPage/LandingPage";

const App = () => {
  const scrollToDiv = (ref) =>
    ref.current.scrollIntoView({ behavior: "smooth" });
  const el1 = useRef();
  const el2 = useRef();
  return (
    <div className="App">
      <LandingPage reference={el1} click={() => scrollToDiv(el2)} />
      <Circle />
      <TextContent1 />
      <TextContent2 />
      <CalculatorApp reference={el2} />
      <ContactUs />
      <Footer />
    </div>
  );
};

export default App;
